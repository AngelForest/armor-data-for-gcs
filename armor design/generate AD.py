import os
import json
import re
from dataclasses import dataclass, field
import csv

from better_csv import BetterDictReader


@dataclass
class Location:
    name: str
    area: float
    location: str
    notes: str


@dataclass
class Material:
    tl: str
    name: str
    weight: float
    cost: float
    construction: str
    notes: list[str] = field(default_factory=list)


def parse_coverage(filename: str) -> list[Location]:
    locations = []
    with open(filename) as f:
        reader = csv.reader(f)
        next(reader)
        for row in reader:
            if not row:
                continue
            location = Location(
                row[0].strip(),
                float(row[1]),
                row[2].strip(),
                row[3].strip()
            )
            locations.append(location)
    return locations


locations_default = parse_coverage('coverage_default.csv')

note_descriptions = {
    'B': 'DR x4 against cut&pi.',
    'C': 'Combustible.',
    'S': 'Semi-Ablative.',
    'E': 'Energy-ablative (ablative against lasers, plasma, fusion or flamers).',
    'M': 'Electromagnetic (only protects against shaped-charge projectiles and plasma bolts).',
    'laser only': 'DR only applies against microwave and visible and near-infrared laser beams.',
    'Biotech': 'Bio-tech (Capable of sealing punctures and rips.)',
}


def process_notes(material: Material, row):
    if not row['Notes']:
        return
    notes = []
    max_dr = int(row['Max DR'])
    pattern_split_dr = re.compile(r'split\(([a-z&]+)/([0-9.]+)\)')
    pattern_min_dr = re.compile(r'min(\d+)')

    text: str
    for text in row['Notes'].split(','):
        if text in note_descriptions:
            notes.append(note_descriptions[text])
        elif text == 'F':
            notes.append(f'Flexible if DR is less than {max_dr // 4}.')
        elif text == 'L':
            notes.append(f'Double DR against shaped-charge warheads and plasma bolts if DR is at least {max_dr // 2}.')
        elif text == 'T':
            # todo: create modifier
            notes.append('<TRANSPARENT>')
            pass
        else:
            match = pattern_split_dr.match(text)
            if match:
                notes.append(
                    f'Full DR against {match.group(1)} only. Divide DR by {match.group(2)} against other types.')
                continue
            match = pattern_min_dr.match(text)
            if match:
                notes.append(f'Armor must be built with at least DR {match.group(1)}.')
                continue
            print('Unexpected note text', text)
            print('row:', row)
    material.notes.extend(notes)


def parse_materials(filename: str) -> list[Material]:
    materials = []
    with open(filename) as f:
        reader = BetterDictReader(f)
        for row in reader:
            if not row:
                continue
            notes = []
            tl = row['TL']
            name = row['Material']
            weight = float(row['WM'])
            cost_raw: str = row['CM']
            if '/' in cost_raw:
                data = cost_raw.split('/')
                cost_raw, data = data[0], data[1:]
                for entry in data:
                    alt_tl, alt_cost = entry.split('$')
                    notes.append(f'Costs {alt_cost} at TL{alt_tl}.')
            cost = float(cost_raw.removeprefix('$'))

            construction: str = row['Construction']
            construction = construction \
                .replace('F/O', 'fabric,optimized fabric') \
                .replace('R/S', 'plate,solid,segmented plate,impact-absorbing plate,scale')

            material = Material(tl, name, weight, cost, construction)
            material.notes.extend(notes)
            process_notes(material, row)

            materials.append(material)
    return materials


def location_to_json(filename: str):
    locations = parse_coverage(filename)

    js_items = list()
    for location in locations:
        hit_locations = location.location.split(',') if location.location else []
        js_items.append(dict(
            name=location.name,
            area=location.area,
            hit_locations=hit_locations,
            notes=location.notes
        ))

    with open(os.path.splitext(filename)[0] + '.json', 'w') as out:
        json.dump(js_items, fp=out, indent='\t')


def materials_to_json():
    materials: list[Material] = list()
    materials.extend(parse_materials('LT materials.csv'))
    materials.extend(parse_materials('HT materials.csv'))
    materials.extend(parse_materials('UT materials.csv'))

    js_items = list()
    for material in materials:
        js_items.append(dict(
            tl=material.tl,
            name=material.name,
            weight=material.weight,
            cost=material.cost,  # todo: support multiple costs
            notes=' '.join(material.notes),
            construction=material.construction.split(',')
        ))

    with open('materials.json', 'w') as out:
        json.dump(js_items, fp=out, indent='\t')


if __name__ == '__main__':
    location_to_json('coverage_default.csv')
    materials_to_json()
