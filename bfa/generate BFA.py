import json
import math
from dataclasses import dataclass
from typing import Tuple, Union, List, Dict, Any
from collections import OrderedDict


@dataclass
class Material:
    name: str
    weight: int
    cost: int
    notes: str


@dataclass
class Location:
    name: str
    percent: int
    location: str
    notes: str = ''


@dataclass
class Mod:
    name: str
    cost: str
    weight: str
    notes: str


materials = (
    Material('Textile', 12, 100, 'Flexible; -2DR vs Imp and Pi'),
    Material('Hide', 13, 60, 'Flexible; -2DR vs Imp and Pi'),
    Material('Bone', 14, 60, ''),
    Material('Plant', 14, 60, 'Combustible'),
    Material('Scale', 10, 250, '-2DR vs Cr'),
    Material('Mail', 6, 500, 'Flexible; -2DR vs Cr'),
    Material('Plate', 7, 700, ''),
)

locations_bfa = (
    Location('Head', 8, 'head'),
    Location('Skull', 3, 'skull'),
    Location('Face', 3, 'face'),
    Location('Neck', 2, 'neck'),
    Location('Torso', 32, 'torso', 'Vitals are hit on a 1 on a d6'),
    Location('Chest', 20, 'chest', 'Vitals are hit on a 1 on a d6'),
    Location('Vitals', 4, 'vitals'),
    Location('Abdomen', 12, 'abdomen', 'Vitals are hit on a 1 on a d6'),
    Location('Groin', 2, 'groin'),
    Location('Arms', 20, 'arm_right,arm_left'),
    Location('Shoulders', 6, 'shoulder_right,shoulder_left', 'Roll 1d; on 1-2 this location is hit'),
    Location('Upper Arms', 4, 'upper_arm_right,upper_arm_left', 'Roll 1d; on 3 this location is hit'),
    Location('Elbows', 4, 'elbow_right,elbow_left', 'Roll 1d; on 4 this location is hit'),
    Location('Forearms', 6, 'forearm_right,forearm_left', 'Roll 1d; on 5-6 this location is hit'),
    Location('Hands', 6, 'hands'),
    Location('Legs', 24, 'leg_right,leg_left'),
    Location('Thighs', 12, 'thigh_right,thigh_left', 'Roll 1d; on 1-3 this location is hit'),
    Location('Knees', 4, 'knee_right,knee_left', 'Roll 1d; on 4 this location is hit'),
    Location('Shins', 8, 'shin_right,shin_left', 'Roll 1d; on 5-6 this location is hit'),
    Location('Feet', 10, 'feet'),
)

locations_default = (
    Location('Head', 8, 'skull,face,neck'),
    Location('Skull', 3, 'skull'),
    Location('Face', 3, 'face'),
    Location('Neck', 2, 'neck'),
    Location('Torso', 32, 'torso,groin', 'Vitals are hit on a 1 on a d6'),
    Location('Chest', 20, '', 'Vitals are hit on a 1 on a d6'),
    Location('Vitals', 4, 'vitals'),
    Location('Abdomen', 12, '', 'Vitals are hit on a 1 on a d6'),
    Location('Groin', 2, 'groin'),
    Location('Arms', 20, 'arm'),
    Location('Shoulders', 6, '', 'Roll 1d; on 1-2 this location is hit'),
    Location('Upper Arms', 4, '', 'Roll 1d; on 3 this location is hit'),
    Location('Elbows', 4, '', 'Roll 1d; on 4 this location is hit'),
    Location('Forearms', 6, '', 'Roll 1d; on 5-6 this location is hit'),
    Location('Hands', 6, 'hand'),
    Location('Legs', 24, 'leg'),
    Location('Thighs', 12, '', 'Roll 1d; on 1-3 this location is hit'),
    Location('Knees', 4, '', 'Roll 1d; on 4 this location is hit'),
    Location('Shins', 8, '', 'Roll 1d; on 5-6 this location is hit'),
    Location('Feet', 10, 'foot'),
)


def parse_armor_mods():
    mods = OrderedDict()
    lines = open('bfa_mods.csv').read().split('\n')
    for line in lines:
        category, name, cost, weight, notes = line.split(',')
        group = mods.setdefault(category, list())
        group.append(Mod(name, cost, weight, notes))
    return mods


armor_mods = parse_armor_mods()


def make_equipment(name: str):
    return dict(type='equipment',
                version=1,
                quantity=1,
                description=name,
                tech_level='',
                reference='',
                modifiers=[],
                notes='')


def add_material_modifiers(equipment: dict, location: Location, material_list: tuple[Material, ...]):
    modifiers = equipment['modifiers']

    dr_modifier = dict(
        type='eqp_modifier',
        version=1,
        name='DR 1',
        cost_type='to_final_cost',
        cost='x1',
        weight_type='to_final_weight',
        weight='x1',
        notes='Adjust DR bonus and multipliers as necessary',
        features=list()
    )
    if location.location:
        for loc in location.location.split(','):
            feature = dict(
                type='dr_bonus',
                amount=1,
                location=loc
            )
            dr_modifier['features'].append(feature)
    modifiers.append(dr_modifier)

    for material in material_list:
        percent = location.percent / 100.0
        modifier = dict(
            type='eqp_modifier',
            version=1,
            disabled=True,
            name=material.name,
            cost_type='to_original_cost',
            cost='+%g' % (material.cost * percent),
            weight_type='to_original_weight',
            weight='+%g lb' % (material.weight * percent)
        )
        modifiers.append(modifier)


def generate_by_location(filename: str, location_list: tuple[Location, ...]):
    root_rows = list()

    for location in location_list:
        equipment = make_equipment(location.name)
        if location.notes:
            equipment['notes'] = location.notes
        add_material_modifiers(equipment, location, materials)

        root_rows.append(equipment)

    root = dict(
        type='equipment_list',
        version=1,
        rows=root_rows
    )
    with open(f'{filename}.eqp', 'w') as out:
        json.dump(root, fp=out, indent='\t')


def generate_extra_armor_mods():
    root_rows = list()

    for category, mods in armor_mods.items():
        category_rows = list()

        for mod in mods:
            category_rows.append(dict(
                type='eqp_modifier',
                name=mod.name,
                cost_type='to_base_cost' if ('cf' in mod.cost) else 'to_original_cost',
                cost=mod.cost,
                weight_type='to_original_weight',
                weight=mod.weight,
                notes=mod.notes
            ))

        root_rows.append(dict(
            type='eqp_modifier_container',
            disabled=True,
            name=category,
            open=True,
            children=category_rows
        ))

    root = dict(
        type='eqp_modifier_list',
        version=2,
        rows=root_rows
    )
    with open('BFA Modifiers.eqm', 'w') as out:
        json.dump(root, fp=out, indent='\t')


if __name__ == '__main__':
    generate_by_location('BFA Armor (BFA body)', locations_bfa)
    generate_by_location('BFA Armor (Default body)', locations_default)
    generate_extra_armor_mods()
