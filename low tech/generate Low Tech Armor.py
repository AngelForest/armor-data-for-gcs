import json
import math
from dataclasses import dataclass
from typing import Tuple, Union, List, Dict, Any


@dataclass
class Material:
    tl: int
    name: str
    dr: str
    cost: int
    weight: int
    don: int
    notes: Union[int, None]
    page: str


@dataclass
class Location:
    name: str
    percent: int
    dr_loc: str
    notes: Union[str, None]
    tl: Union[int, None] = None
    page: Union[str, None] = None


materials = (
    Material(0, "Cane", "1", 35, 12, 28, 1, 'LT106'),
    Material(0, "Cloth, Padded", "1*", 50, 6, 15, None, 'LT103'),
    Material(0, "Horn", "3", 250, 25, 30, None, 'LT106'),
    Material(0, "Layered Cloth, Light", "2*", 150, 12, 20, None, 'LT103'),
    Material(0, "Layered Cloth, Medium", "3", 350, 20, 30, None, 'LT103'),
    Material(0, "Layered Cloth, Heavy", "4", 600, 28, 30, None, 'LT103'),
    Material(0, "Leather, Medium", "2*", 100, 12, 30, 2, 'LT104'),
    Material(0, "Leather, Heavy", "3", 200, 20, 30, 2, 'LT104'),
    Material(0, "Straw", "2", 50, 20, 30, 1, 'LT106'),
    Material(0, "Wood", "3", 100, 30, 30, 3, 'LT106'),
    Material(1, "Layered Leather, Light", "2*", 120, 15, 20, None, 'LT105'),
    Material(1, "Layered Leather, Medium", "3", 220, 26, 30, None, 'LT105'),
    Material(1, "Layered Leather, Heavy", "4", 525, 35, 30, None, 'LT105'),
    Material(1, "Scale, Light", "3", 320, 16, 30, 4, 'LT107'),
    Material(1, "Scale, Medium", "4", 550, 28, 30, 4, 'LT107'),
    Material(1, "Scale, Heavy", "5", 1100, 40, 30, None, 'LT107'),
    Material(2, "Hardened Leather, Medium", "2", 125, 15, 30, None, 'LT105'),
    Material(2, "Hardened Leather, Heavy", "3", 250, 25, 30, None, 'LT105'),
    Material(2, "Jack of Plates", "3", 300, 18, 30, 4, 'LT107'),
    Material(2, "Mail, Light", "3*", 500, 12, 15, 5, 'LT107'),
    Material(2, "Mail, Fine", "4*", 900, 15, 15, 5, 'LT107'),
    Material(2, "Mail, Heavy", "5*", 1200, 18, 15, 5, 'LT107'),
    Material(2, "Segmented Plate, Light", "3", 600, 16, 45, None, 'LT107'),
    Material(2, "Segmented Plate, Medium", "4", 900, 24, 45, None, 'LT107'),
    Material(2, "Segmented Plate, Heavy", "5", 1200, 32, 45, None, 'LT107'),
    Material(3, "Mail and Plates", "5", 1000, 20, 20, 4, 'LT107'),
    Material(3, "Mail, Jousting", "6", 1500, 30, 30, 6, 'LT107'),
    Material(4, "Brigandine, Light", "3", 900, 10, 30, None, 'LT108'),
    Material(4, "Brigandine, Medium", "5", 1800, 20, 30, None, 'LT108'),
    Material(4, "Paper, Proofed", "6", 2000, 45, 20, 1, 'LT106'),
    Material(4, "Plate, Light", "3", 1000, 8, 45, None, 'LT108'),
    Material(4, "Plate, Medium", "6", 2500, 20, 45, None, 'LT108'),
    Material(4, "Plate, Heavy", "9", 4000, 32, 45, None, 'LT108'),
)

mat_notes = [
    'PLACEHOLDER',
    'Combustible. If DR is penetrated by burning damage, it can catch fire. See Making Things Burn (p. B433); treat '
    'the armor material as resistant',
    '-1 DR vs. impaling',
    'Semi-ablative. Loses 1 DR per 10 points of basic damage it resists (see p. B47)',
    '-1 DR vs. crushing',
    '-2 DR vs. crushing',
    '-1 DX, except for Lance skill',
]

locations = (
    Location('Head', 30, 'skull,face', None),
    Location('Skull', 20, 'skull', None),
    Location('Face', 10, 'face', None),
    Location('Neck', 5, 'neck', None),
    Location('Torso', 100, 'torso', 'Roll 1d; on 1, the vitals are hit.'),
    Location('Chest', 75, '', 'Hit location 9-10. Roll 1d; on 1, the vitals are hit.', page='LT102'),
    Location('Abdomen', 75, '', 'Hit location 11. Roll 1d; on 1, the vitals are hit.', page='LT102'),
    Location('Groin', 5, 'groin', None),
    Location('Arms', 50, 'arms', None),
    Location('Shoulders', 10, '', 'Roll 1d; on 6, the armor is hit.'),
    Location('Upper Arms', 10, '', 'Roll 1d; on 5, the armor is hit.'),
    Location('Elbows', 5, '', 'Roll 1d; on 4, the armor is hit.'),
    Location('Forearms', 25, '', 'Roll 1d; on 1-3, the armor is hit.'),
    Location('Hands', 10, 'hands', None),
    Location('Legs', 100, 'legs', None),
    Location('Thighs', 45, '', 'Roll 1d; on 5-6, the armor is hit.'),
    Location('Knees', 5, '', 'Roll 1d; on 4, the armor is hit.'),
    Location('Shins', 50, '', 'Roll 1d; on 1-3, the armor is hit.'),
    Location('Feet', 10, 'feet', None),
)

face_protection = (
    Location('Nasal', 5, '', '+1/6', page='LT112'),
    Location('Brim', 15, '', '+1/6; +5/6 from above', page='LT112'),
    Location('Cheek Guards', 15, '', '+2/6', page='LT112'),
    Location('Full Cheek Guards', 15, '', '+3/6; gives Hard of Hearing', page='LT112'),
    Location('Spectacles', 5, '', '+1/6; gives No Peripheral Vision', page='LT112'),
    Location('Visor', 25, '', '+5/6; gives Hard of Hearing and No Peripheral Vision', page='LT112'),
)

neck_protection = (
    Location('Turret', 10, 'neck', "+2/6 chance to protect face; can't look down", 1, page='LT113'),
    Location('Aventail', 5, 'neck', None, 2, page='LT113'),
    Location('Lobsterback', 3, '', 'Protects neck from behind', 2, page='LT113'),
    Location('Mail Collar', 5, 'neck', None, 2, page='LT113'),
    Location('Standard', 20, 'neck', '50% DR to hit location 9', 2, page='LT113'),
    Location('Ventail', 3, '', 'Protects neck from front; +2/6 chance to protect face', 2, page='LT113'),
    Location('Bevor', 7, 'neck', '+1/6 chance to protect face', 3, page='LT113'),
    Location('Gorget', 5, 'neck', None, 3, page='LT113'),
)


def make_equipment(name: str, tl, page: str):
    return dict(type='equipment',
                version=1,
                quantity=1,
                description=name,
                tech_level=str(tl),
                reference=page,
                modifiers=[],
                notes='')


def add_locations(equipment, material: Material, location_list: Tuple[Location, ...], add_don_time=False):
    modifiers = equipment['modifiers']
    dr = int(material.dr.rstrip('*'))
    for location in location_list:
        percent = location.percent / 100.0
        modifier = dict(type='eqp_modifier',
                        version=1,
                        disabled=True,
                        name=location.name,
                        cost_type='to_original_cost',
                        cost='+%g' % (material.cost * percent),
                        weight_type='to_original_weight',
                        weight='+%g lb' % (material.weight * percent))
        if location.page:
            modifier['reference'] = location.page
        if location.tl:
            modifier['tech_level'] = str(location.tl)
        if location.dr_loc:
            modifier['features'] = []
            for dr_loc in location.dr_loc.split(','):
                feature = dict(type='dr_bonus',
                               amount=dr,
                               location=dr_loc)
                modifier['features'].append(feature)
        note = ''
        if location.notes:
            note = location.notes
        if add_don_time:
            if note:
                note += '; '
            note += 'Don time: %d secs.' % int(math.ceil(material.don * percent))
        modifier['notes'] = note
        modifiers.append(modifier)


def generate_by_material():
    root = dict(type='equipment_list',
                version=1,
                rows=[])
    root_rows = root['rows']

    face_protection_list = dict(type='equipment_container',
                                version=1,
                                description='Face Protection',
                                reference='LT112',
                                open=True,
                                children=[])
    neck_protection_list = dict(type='equipment_container',
                                version=1,
                                description='Neck Protection',
                                reference='LT113',
                                open=True,
                                children=[])

    for material in materials:
        dr = int(material.dr.rstrip('*'))
        flexible = material.dr.endswith('*')
        equipment = make_equipment(material.name, material.tl, material.page)
        equipment['notes'] += 'DR %d; ' % dr
        add_locations(equipment, material, locations, True)
        equipment['modifiers'].append(dict(type='eqp_modifier',
                                           version=1,
                                           disabled=True,
                                           name='@Covers only half (front only, one arm, etc)@',
                                           reference='LT100',
                                           cost_type='to_base_cost',
                                           cost='x0.5',
                                           weight_type='to_base_weight',
                                           weight='x0.5'))
        if flexible:
            equipment['notes'] += 'Flexible, susceptible to blunt trauma; '
        if material.notes:
            equipment['notes'] += mat_notes[int(material.notes)] + '; '
        equipment['notes'] += 'Holdout: %d.' % (-int(math.ceil(dr / 3.0) if flexible else dr))

        root_rows.append(equipment)

        # face protection
        equipment = make_equipment(material.name, material.tl, material.page)
        add_locations(equipment, material, face_protection)
        equipment['notes'] += 'Roll 1d. If result is equal to or less than total protection, the armor is hit.'
        face_protection_list['children'].append(equipment)

        # neck protection
        equipment = make_equipment(material.name, material.tl, material.page)
        add_locations(equipment, material, neck_protection)
        neck_protection_list['children'].append(equipment)

    root_rows.append(face_protection_list)
    root_rows.append(neck_protection_list)

    with open("Low Tech Armor (by material).eqp", "w") as out:
        json.dump(root, fp=out, indent='\t')


def generate_by_location():
    root = dict(type='equipment_list',
                version=1,
                rows=[])
    root_rows = root['rows']

    for location in locations + (
            Location('Face, Partial', 100, '',
                     'Roll 1d. If result is equal to or less than total protection, the armor is hit.',
                     page='LT112'),
            Location('Neck, Partial', 100, '', None, page='LT113')):
        percent = location.percent / 100.0
        is_partial = location.name.endswith('Partial')
        equipment: Dict[str, Any] = dict(type='equipment',
                                         version=1,
                                         quantity=1,
                                         description=location.name,
                                         modifiers=[])
        if location.page:
            equipment['reference'] = location.page
        if location.notes:
            equipment['notes'] = location.notes

        modifiers: List = equipment['modifiers']

        for material in materials:
            dr = int(material.dr.rstrip('*'))
            flexible = material.dr.endswith('*')
            modifier = dict(type='eqp_modifier',
                            version=1,
                            disabled=True,
                            name=material.name,
                            tech_level=str(material.tl),
                            reference=material.page,
                            cost_type='to_original_cost',
                            cost='+%g' % (material.cost * percent),
                            weight_type='to_original_weight',
                            weight='+%g lb' % (material.weight * percent))
            if is_partial:
                modifier.update(cost_type='to_base_cost',
                                cost='x%g' % (material.cost * percent),
                                weight_type='to_base_weight',
                                weight='x%g' % (material.weight * percent))
            note = 'DR %s; ' % material.dr
            if material.notes:
                note += mat_notes[int(material.notes)] + '; '
            if not is_partial:
                note += 'Don time: %d secs; ' % int(math.ceil(material.don * percent))
            note += 'Holdout %d.' % (-int(math.ceil(dr / 3.0) if flexible else dr))
            modifier['notes'] = note

            if location.dr_loc:
                modifier['features'] = []
                for dr_loc in location.dr_loc.split(','):
                    feature = dict(type='dr_bonus',
                                   amount=dr,
                                   location=dr_loc)
                    modifier['features'].append(feature)
            modifiers.append(modifier)

        if not is_partial:
            equipment['modifiers'].append(dict(type='eqp_modifier',
                                               version=1,
                                               disabled=True,
                                               name='@Covers only half (front only, one arm, etc)@',
                                               reference='LT100',
                                               cost_type='to_base_cost',
                                               cost='x0.5',
                                               weight_type='to_base_weight',
                                               weight='x0.5'))

        if is_partial:
            equipment['value'] = '1'
            equipment['weight'] = '1 lb'

        def generate_partial_locations(partial_list: Tuple[Location, ...]):
            modifiers.append(dict(type='eqp_modifiers',
                                  version=1,
                                  disabled=False,
                                  name='Partial',
                                  cost_type='to_original_cost',
                                  cost='-100%',
                                  weight_type='to_original_weight',
                                  weight='-100%'))
            for partial in partial_list:
                mod = dict(type='eqp_modifier',
                           version=1,
                           disabled=True,
                           name=partial.name,
                           tech_level=partial.tl,
                           cost_type='to_original_cost',
                           cost='+%g%%' % partial.percent,
                           weight_type='to_original_weight',
                           weight='+%g%%' % partial.percent)
                if partial.notes:
                    mod['notes'] = partial.notes
                modifiers.append(mod)

        if location.name == 'Face, Partial':
            generate_partial_locations(face_protection)

        if location.name == 'Neck, Partial':
            generate_partial_locations(neck_protection)

        root_rows.append(equipment)

    with open("Low Tech Armor (by location).eqp", "w") as out:
        json.dump(root, fp=out, indent='\t')


if __name__ == '__main__':
    generate_by_material()
    generate_by_location()
